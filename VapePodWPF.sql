﻿use master
go
drop database if exists VapePodStore
go
create database VapePodStore
go
use VapePodStore
go

-- Create Account table for VapePodStore
CREATE TABLE Account (
    AccountID INT IDENTITY(1,1) PRIMARY KEY,
    AccountName NVARCHAR(100) NOT NULL,
    Address NVARCHAR(100) NOT NULL,
    PhoneNumber CHAR(10) NOT NULL,
    BirthDay NVARCHAR(100) NOT NULL,
    Email CHAR(100) UNIQUE,
    StatusAccount BIT,
    Password VARCHAR(50),
    Role INT
);

-- Create VapeProduct table for VapePodStore
CREATE TABLE VapeProduct (
    ProductID INT IDENTITY(1,1) PRIMARY KEY,
    ProductName VARCHAR(100) NOT NULL,
    Flavor VARCHAR(100),
    Description NVARCHAR(100),
    NicotineLevel FLOAT NOT NULL,
    Quantity INT NOT NULL,
    Price DECIMAL NOT NULL,
    StatusProduct BIT,
    DateAddProduct DATETIME
);

-- Create OrderVape table for VapePodStore
CREATE TABLE OrderVape (
    OrderID INT IDENTITY(1,1) PRIMARY KEY,
    Date DATE NOT NULL,
    Website CHAR(100) DEFAULT N'VapePodStore',
    OrderAddress NVARCHAR(100) NULL,
    AccountID INT FOREIGN KEY REFERENCES Account(AccountID),
    StatusOrderVape BIT
);

-- Create OrderVapeDetail table for VapePodStore
CREATE TABLE OrderVapeDetail (
    Name CHAR(100) NOT NULL,
    Price DECIMAL NOT NULL,
    Quantity INT,
    NicotineLevel FLOAT NOT NULL,
    ProductID INT FOREIGN KEY REFERENCES VapeProduct(ProductID),
    OrderID INT FOREIGN KEY REFERENCES OrderVape(OrderID),
    PRIMARY KEY (OrderID, ProductID),
    StatusOrderDetail BIT
);

-- Insert sample data into Account table
INSERT INTO Account (AccountName, Address, PhoneNumber, BirthDay, Email, StatusAccount)
VALUES
    (N'Nguyễn Trần Minh', N'123 Thanh Quận', '0933627492', '11/12/1999', 'minhnt@gmail.com', 1),
    (N'Ngô Toàn Hưng', N'524 Trần Đình Xu', '0901223859', '15/1/1988', 'hungnt@gmail.com', 1),
    (N'Pham Cao Hoang Minh', N'333/98, Tran Binh Trong', '0123456778', '29/7/2001', 'Minhpew@gmail.com', 1);

-- Insert sample data into VapeProduct table
INSERT INTO VapeProduct (ProductName, Flavor, Description, NicotineLevel, Quantity, Price, StatusProduct, DateAddProduct)
VALUES
    ('Mango Bliss', 'Mango', N'Sweet and juicy mango flavor', 3, 50, 250000, 1, '2023-11-21T14:00:00'),
    ('Cool Mint', 'Mint', N'Refreshing cool mint flavor', 2, 40, 220000, 1, '2023-11-21T14:00:00'),
    ('Strawberry Lime', 'Strawberry', N'Sweet and cool with strawberry flavor', 2, 40, 230000, 1, '2023-11-25T14:00:00');

-- Insert sample data into OrderVape table
INSERT INTO OrderVape (Date, OrderAddress, AccountID, StatusOrderVape)
VALUES
    ('2023-11-21', '123 Main Street', 1, 1),
    ('2023-11-22', '456 Oak Avenue', 2, 1),
    ('2023-11-23', '333/79 Nguyen Trai Street', 3, 1);

-- Insert sample data into OrderVapeDetail table
INSERT INTO OrderVapeDetail (Name, Price, Quantity, NicotineLevel, ProductID, OrderID, StatusOrderDetail)
VALUES
    ('Mango Bliss', 250000, 3, 3, 1, 1, 1),
    ('Cool Mint', 220000, 2, 2, 2, 1, 1),
    ('Strawberry', 230000, 5, 4, 3, 1, 1);
