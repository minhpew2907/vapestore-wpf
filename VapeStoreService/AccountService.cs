﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;
using VapeStoreDAO;

namespace VapeStoreService
{
    public class AccountService : IAccountservice
    {
        private AccountDAO userAccount;

        public AccountService()
        {
            userAccount = new AccountDAO();
        }

        public void Add(Account account)
        {
            userAccount.AddUser(account);
        }

        public void Delete(Account account)
        {
            userAccount.DeleteUser(account);
        }

        public Account GetAccountbyId(int id)
        {
            return userAccount.GetAccountUserById(id);
        }

        public List<Account> GetAllAccounts()
        {
            return userAccount.GetAccounts();
        }

        public Account Login(string email, string password)
        {
            return userAccount.Login(email, password);
        }

        public IQueryable<Account> SearchAccount(string name)
        {
            return userAccount.SearchAccountByName(name);
        }

        public void Update(Account account)
        {
            userAccount.UpdateUser(account);
        }
    }
}
