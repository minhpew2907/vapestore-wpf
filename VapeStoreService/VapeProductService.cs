﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;
using VapeStoreDAO;

namespace VapeStoreService
{
    public class VapeProductService : IVapeProductService
    {
        private VapeProductDAO vapeProductDAO;

        public VapeProductService()
        {
            vapeProductDAO = new VapeProductDAO();
        }

        public void Add(VapeProduct vapeProduct)
        {
            vapeProductDAO.AddVape(vapeProduct);
        }

        public void Delete(VapeProduct vapeProduct)
        {
            vapeProductDAO.DeleteVape(vapeProduct);
        }

        public VapeProduct GetVapeById(int id)
        {
            return vapeProductDAO.GetVapeById(id);
        }

        public List<VapeProduct> GetVapeProducts()
        {
            return vapeProductDAO.GetAllVapeProducts();
        }

        public IQueryable<VapeProduct> SearchVape(string name)
        {
            return vapeProductDAO.SearchVapeByName(name);
        }

        public void Update(VapeProduct vapeProduct)
        {
            vapeProductDAO.UpdateVape(vapeProduct);
        }
    }
}
