﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;

namespace VapeStoreService
{
    public interface IAccountservice
    {
        public Account Login(string email, string password);
        public List<Account> GetAllAccounts();
        public void Add(Account account);
        public void Update(Account account);
        public void Delete(Account account);
        public Account GetAccountbyId(int id);
        public IQueryable<Account> SearchAccount(string name);
    }
}
