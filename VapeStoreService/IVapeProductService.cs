﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;
using VapeStoreDAO;

namespace VapeStoreService
{
    public interface IVapeProductService
    {
        public List<VapeProduct> GetVapeProducts();
        public void Add(VapeProduct vapeProduct);
        public void Update(VapeProduct vapeProduct);
        public void Delete(VapeProduct vapeProduct);
        public VapeProduct GetVapeById(int id);
        public IQueryable<VapeProduct> SearchVape(string name);
    }
}
