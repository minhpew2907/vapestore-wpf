﻿using System;
using System.Collections.Generic;

namespace VapeStoreBO
{
    public partial class Account
    {
        public Account()
        {
            OrderVapes = new HashSet<OrderVape>();
        }

        public int AccountId { get; set; }
        public string AccountName { get; set; } = null!;
        public string Address { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string BirthDay { get; set; } = null!;
        public string? Email { get; set; }
        public bool? StatusAccount { get; set; }
        public string? Password { get; set; }
        public int? Role { get; set; }

        public virtual ICollection<OrderVape> OrderVapes { get; set; }
    }
}
