﻿using System;
using System.Collections.Generic;

namespace VapeStoreBO
{
    public partial class OrderVapeDetail
    {
        public string Name { get; set; } = null!;
        public decimal Price { get; set; }
        public int? Quantity { get; set; }
        public double NicotineLevel { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public bool? StatusOrderDetail { get; set; }

        public virtual OrderVape Order { get; set; } = null!;
        public virtual VapeProduct Product { get; set; } = null!;
    }
}
