﻿using System;
using System.Collections.Generic;

namespace VapeStoreBO
{
    public partial class OrderVape
    {
        public OrderVape()
        {
            OrderVapeDetails = new HashSet<OrderVapeDetail>();
        }

        public int OrderId { get; set; }
        public DateTime Date { get; set; }
        public string? Website { get; set; }
        public string? OrderAddress { get; set; }
        public int? AccountId { get; set; }
        public bool? StatusOrderVape { get; set; }

        public virtual Account? Account { get; set; }
        public virtual ICollection<OrderVapeDetail> OrderVapeDetails { get; set; }
    }
}
