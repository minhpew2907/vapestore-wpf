﻿using System;
using System.Collections.Generic;

namespace VapeStoreBO
{
    public partial class VapeProduct
    {
        public VapeProduct()
        {
            OrderVapeDetails = new HashSet<OrderVapeDetail>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; } = null!;
        public string? Flavor { get; set; }
        public string? Description { get; set; }
        public double NicotineLevel { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool? StatusProduct { get; set; }
        public DateTime? DateAddProduct { get; set; }

        public virtual ICollection<OrderVapeDetail> OrderVapeDetails { get; set; }
    }
}
