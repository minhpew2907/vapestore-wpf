﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;
using VapeStoreDAO;

namespace VapeStoreRepo
{
    public interface IVapeProductRepo
    {
        public List<VapeProduct> GetVapeProducts();
        public void Add(VapeProduct vapeProduct);
        public void Update(VapeProduct vapeProduct);
        public void Delete(VapeProduct vapeProduct);
        public VapeProduct GetVapeProductbyId(int id);
        public IQueryable<VapeProduct> SearchVapeProduct(string name);
    }
}
