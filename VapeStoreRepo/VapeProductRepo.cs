﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;
using VapeStoreDAO;

namespace VapeStoreRepo
{
    public class VapeProductRepo : IVapeProductRepo
    {
        private VapeProductDAO vapeProductDAO;

        public VapeProductRepo()
        {
            vapeProductDAO = new VapeProductDAO();
        }

        public void Add(VapeProduct vapeProduct)
        {
            vapeProductDAO.AddVape(vapeProduct);
        }

        public void Delete(VapeProduct vapeProduct)
        {
            vapeProductDAO.DeleteVape(vapeProduct);
        }

        public VapeProduct GetVapeProductbyId(int id)
        {
            return vapeProductDAO.GetVapeById(id);
        }

        public List<VapeProduct> GetVapeProducts()
        {
            return vapeProductDAO.GetAllVapeProducts();
        }

        public IQueryable<VapeProduct> SearchVapeProduct(string name)
        {
            return vapeProductDAO.SearchVapeByName(name);
        }

        public void Update(VapeProduct vapeProduct)
        {
            vapeProductDAO.UpdateVape(vapeProduct);
        }
    }
}
