﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using VapeStoreService;

namespace VapePodManagementWPF
{
    public partial class MainWindow : Window
    {
        private readonly IAccountservice accountService;

        public MainWindow()
        {
            accountService = new AccountService();
            InitializeComponent();
        }

        private void textEmail_MouseDown(object sender, MouseButtonEventArgs e)
        {
            txtEmail.Focus();
        }

        //private void txtEmail_TextChanged(object sender, TextChangedEventArgs e)
        //{

        //    if (!string.IsNullOrEmpty(txtEmail.Text) && txtEmail.Text.Length > 0)
        //    {
                
        //    }
        //}

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            string email = txtEmail.Text;
            string password = txtPassword.Password;
            var userAccount = accountService.Login(email, password);
            if (userAccount is not null)
            {
                ChoosingToDirect choosingToDirect = new ChoosingToDirect();
                choosingToDirect.ShowDialog();
                this.Close();
                MessageBox.Show("Login successful!");
            }
            else
            {
                MessageBox.Show("Invalid email or password. Please try again.");
            }
        }
    }
}
