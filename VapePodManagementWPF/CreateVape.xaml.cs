﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VapeStoreBO;
using VapeStoreService;

namespace VapePodManagementWPF
{
    /// <summary>
    /// Interaction logic for CreateVape.xaml
    /// </summary>
    public partial class CreateVape : Window
    {
        private readonly IVapeProductService productService;

        public CreateVape()
        {
            productService = new VapeProductService();
            InitializeComponent();
        }

        private VapeProduct GetVapeObject()
        {
            VapeProduct product = new VapeProduct();
            product.ProductName = txtName.Text;
            product.Flavor = txtFlavor.Text;
            product.Description = txtDescription.Text;
            product.NicotineLevel = double.Parse(txtNicotineLevel.Text);
            product.Quantity = int.Parse(txtQuantity.Text);
            product.Price = decimal.Parse(txtPrice.Text);
            product.StatusProduct = bool.Parse(txtStatus.Text);
            product.DateAddProduct = DateTime.Parse(dtDateAddVapeProduct.Text);
            return product;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var createVapeProduct = GetVapeObject();
            productService.Add(createVapeProduct);
            VapeManagement vapeManagement = new VapeManagement();
            vapeManagement.lviewVapeProduct.ItemsSource = productService.GetVapeProducts();
            vapeManagement.Show();
            this.Close();
        }
    }
}
