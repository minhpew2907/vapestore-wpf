﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VapePodManagementWPF
{
    /// <summary>
    /// Interaction logic for ChoosingToDirect.xaml
    /// </summary>
    public partial class ChoosingToDirect : Window
    {
        public ChoosingToDirect()
        {
            InitializeComponent();
        }

        private void btnUser_Click(object sender, RoutedEventArgs e)
        {
            UserAccountManagement userAccountManagement = new UserAccountManagement();
            userAccountManagement.ShowDialog();
        }

        private void btnVapes_Click(object sender, RoutedEventArgs e)
        {
            VapeManagement vapeManagement = new VapeManagement();
            vapeManagement.ShowDialog();
        }

        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
