﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VapeStoreBO;
using VapeStoreService;

namespace VapePodManagementWPF
{
    public partial class CreateAccount : Window
    {
        private readonly IAccountservice accountService;

        public CreateAccount()
        {
            accountService = new AccountService();
            InitializeComponent();
        }

        private Account GetAccountObject()
        {
            Account account = new Account();
            account.AccountName = txtName.Text;
            account.Address = txtAddress.Text;
            account.PhoneNumber = txtPhone.Text;
            account.BirthDay = txtDOB.Text;
            account.Email = txtEmail.Text;
            account.StatusAccount = bool.Parse(txtStatus.Text);
            return account;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var createAccount = GetAccountObject();
            accountService.Add(createAccount);
            UserAccountManagement userAccountManagement = new UserAccountManagement();
            userAccountManagement.lviewUserAccount.ItemsSource = accountService.GetAllAccounts();
            userAccountManagement.Show();
            this.Close();
        }
    }
}
