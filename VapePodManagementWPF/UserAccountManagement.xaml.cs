﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VapeStoreBO;
using VapeStoreService;

namespace VapePodManagementWPF
{
    /// <summary>
    /// Interaction logic for UserAccountManagement.xaml
    /// </summary>
    public partial class UserAccountManagement : Window
    {
        private readonly IAccountservice accountService;

        public UserAccountManagement()
        {
            accountService = new AccountService();
            lviewUserAccount = new ListView();
            lviewUserAccount.ItemsSource = accountService.GetAllAccounts();
            InitializeComponent();
        }

        public void loadForm()
        {
            lviewUserAccount.ItemsSource = accountService.GetAllAccounts();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            lviewUserAccount.ItemsSource = accountService.GetAllAccounts();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            CreateAccount createAccount = new CreateAccount();
            createAccount.Show();
            this.Close();
        }

        private Account GetAccountObject()
        {
            Account account = new Account();
            account.AccountName = txtName.Text;
            account.Address = txtAddress.Text;
            account.PhoneNumber = txtPhonenumber.Text;
            account.BirthDay = txtDOB.Text;
            account.Email = txtEmail.Text;
            account.StatusAccount = bool.Parse(txtStatusAccount.Text);
            return account;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var userId = txtID.Text;
            if (userId is null)
            {
                MessageBox.Show("Data is empty");
            }
            else
            {
                MessageBoxButton messageBoxButton = MessageBoxButton.OKCancel;
                MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to update this", "Yes", messageBoxButton);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    accountService.Update(new Account
                    {
                        AccountId = int.Parse(userId),
                        AccountName = txtName.Text,
                        Address = txtAddress.Text,
                        PhoneNumber = txtPhonenumber.Text,
                        BirthDay = txtDOB.Text,
                        Email = txtEmail.Text,
                        StatusAccount = bool.Parse(txtStatusAccount.Text)
                    });
                    loadForm();
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var userId = txtID.Text;
            if (userId is null)
            {
                MessageBox.Show("Data is empty");
            }
            else
            {
                MessageBoxButton messageBoxButton = MessageBoxButton.OKCancel;
                MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to delete this", "Yes", messageBoxButton);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    var accountId = accountService.GetAccountbyId(Int32.Parse(userId));
                    if (accountId is not null)
                    {
                        accountService.Delete(accountId);
                        loadForm();
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            var searchAccount = txtSearch.Text;
            if (searchAccount is not null && searchAccount.Trim().Length >= 1)
            {
                lviewUserAccount.ItemsSource = accountService.SearchAccount(searchAccount).ToList();
            }
            else
            {
                loadForm();
            }
        }
    }
}
