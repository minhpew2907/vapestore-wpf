﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VapeStoreBO;
using VapeStoreService;

namespace VapePodManagementWPF
{
    /// <summary>
    /// Interaction logic for VapeManagement.xaml
    /// </summary>
    public partial class VapeManagement : Window
    {
        private readonly IVapeProductService vapeProduct;

        public VapeManagement()
        {
            vapeProduct = new VapeProductService();
            lviewVapeProduct = new ListView();
            lviewVapeProduct.ItemsSource = vapeProduct.GetVapeProducts();
            InitializeComponent();
        }

        public void loadForm()
        {
            lviewVapeProduct.ItemsSource = vapeProduct.GetVapeProducts();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            lviewVapeProduct.ItemsSource = vapeProduct.GetVapeProducts();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            CreateVape createVape = new CreateVape();
            createVape.Show();
            this.Close();
        }

        private VapeProduct GetVapeObject()
        {
            VapeProduct vapeProduct = new VapeProduct();
            vapeProduct.ProductName = txtName.Text;
            vapeProduct.Flavor = txtFlavor.Text;
            vapeProduct.Description = txtDescription.Text;
            vapeProduct.NicotineLevel = double.Parse(txtNicotineLevel.Text);
            vapeProduct.Quantity = int.Parse(txtQuantity.Text);
            vapeProduct.Price = decimal.Parse(txtPrice.Text);
            vapeProduct.StatusProduct = bool.Parse(txtStatusVape.Text);
            vapeProduct.DateAddProduct = DateTime.Parse(dtDateAddProduct.Text);
            return vapeProduct;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var vapeProductId = txtID.Text;
            if (vapeProductId is null)
            {
                MessageBox.Show("Data is empty");
            }
            else
            {
                MessageBoxButton messageBoxButton = MessageBoxButton.OKCancel;
                MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to update this", "Yes", messageBoxButton);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    vapeProduct.Update(new VapeProduct
                    {
                        ProductId = int.Parse(vapeProductId),
                        ProductName = txtName.Text,
                        Flavor = txtFlavor.Text,
                        Description = txtDescription.Text,
                        NicotineLevel = double.Parse(txtNicotineLevel.Text),
                        Quantity = int.Parse(txtQuantity.Text),
                        Price = decimal.Parse(txtPrice.Text),
                        StatusProduct = bool.Parse(txtStatusVape.Text),
                        DateAddProduct = DateTime.Parse(dtDateAddProduct.Text)
                    });
                    loadForm();
                }
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var vapeProductId = txtID.Text;
            if (vapeProductId is null)
            {
                MessageBox.Show("Data is empty");
            }
            else
            {
                MessageBoxButton messageBoxButton = MessageBoxButton.OKCancel;
                MessageBoxResult messageBoxResult = MessageBox.Show("Do you want to delete this", "Yes", messageBoxButton);
                if (messageBoxResult == MessageBoxResult.OK)
                {
                    var productId = vapeProduct.GetVapeById(int.Parse(vapeProductId));
                    if (productId is not null)
                    {
                        vapeProduct.Delete(productId);
                        loadForm();
                    }
                }
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            var searchVapeProduct = txtSearch.Text;
            if (searchVapeProduct is not null && searchVapeProduct.Trim().Length >= 1)
            {
                lviewVapeProduct.ItemsSource = vapeProduct.SearchVape(searchVapeProduct).ToList();
            }
            else
            {
                loadForm();
            }
        }
    }
}
