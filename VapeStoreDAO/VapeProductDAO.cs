﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;

namespace VapeStoreDAO
{
    public class VapeProductDAO
    {
        private readonly VapePodStoreContext _context;

        public VapeProductDAO()
        {
            _context = new VapePodStoreContext();
        }

        public List<VapeProduct> GetAllVapeProducts()
        {
            return _context.VapeProducts.ToList();
        }

        public bool AddVape(VapeProduct vapeProduct)
        {
            var vapePod = _context.VapeProducts.FirstOrDefault(x => x.ProductId == vapeProduct.ProductId);
            if (vapePod is not null)
            {
                return false;
            }
            else
            {
                _context.VapeProducts.Add(vapeProduct);
                _context.SaveChanges();
                return true;
            }
        }

        public bool UpdateVape(VapeProduct vapeProduct)
        {
            var vapePod = _context.VapeProducts.FirstOrDefault(x => x.ProductId == vapeProduct.ProductId);
            if (vapePod is null)
            {
                return false;
            }
            else
            {
                _context.Entry(vapePod).CurrentValues.SetValues(vapeProduct);
                _context.SaveChanges();
                return true;
            }
        }

        public VapeProduct GetVapeById(int id)
        {
            return _context.VapeProducts.FirstOrDefault(x => x.ProductId == id);
        }

        public void DeleteVape(VapeProduct vapeProduct)
        {
            var vapePod = _context.VapeProducts.FirstOrDefault(x => x.ProductId == vapeProduct.ProductId);
            _context.VapeProducts.Remove(vapePod);
            _context.SaveChanges();
        }

        public IQueryable<VapeProduct> SearchVapeByName(string nameSearch)
        {
            var searchVape = _context.VapeProducts.Where(x => x.ProductName.ToUpper().Contains(nameSearch.Trim().ToUpper()));
            return searchVape;
        }
    }
}
