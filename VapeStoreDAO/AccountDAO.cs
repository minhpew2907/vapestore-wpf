﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VapeStoreBO;

namespace VapeStoreDAO
{
    public class AccountDAO
    {
        private readonly VapePodStoreContext _context;

        public AccountDAO()
        {
            _context = new VapePodStoreContext();
        }

        public Account Login(string email, string password)
        {
            var userAccount = _context.Accounts.FirstOrDefault(x => x.Email.Equals(email.Trim()) && x.Password.Equals(password.Trim()));
            return userAccount;
        }

        public List<Account> GetAccounts()
        {
            return _context.Accounts.ToList();
        }

        public bool AddUser(Account account)
        {
            var userAccount = _context.Accounts.FirstOrDefault(x => x.AccountId == account.AccountId);
            if (userAccount is not null)
            {
                return false;
            }
            else
            {
                _context.Accounts.Add(account);
                _context.SaveChanges();
                return true;
            }
        }

        public bool UpdateUser(Account account)
        {
            var userAccount = _context.Accounts.FirstOrDefault(x => x.AccountId == account.AccountId);
            if (userAccount is null)
            {
                return false;
            }
            else
            {
                _context.Entry(userAccount).CurrentValues.SetValues(account);
                _context.SaveChanges();
                return true;
            }
        }

        public Account GetAccountUserById(int id)
        {
            return _context.Accounts.FirstOrDefault(x => x.AccountId == id);
        }

        public void DeleteUser(Account account)
        {
            var userAccount = _context.Accounts.FirstOrDefault(x => x.AccountId == account.AccountId);
            _context.Accounts.Remove(userAccount);
            _context.SaveChanges();
        }

        public IQueryable<Account> SearchAccountByName(string nameSearch)
        {
            var userAccount = _context.Accounts.Where(x => x.AccountName.ToUpper().Contains(nameSearch.Trim().ToUpper()));
            return userAccount;
        }
    }
}
